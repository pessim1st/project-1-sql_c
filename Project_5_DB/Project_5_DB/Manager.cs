﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_5_DB
{
    public partial class Manager : Form
    {
        string connection_string = "Server=tcp:itacademy.database.windows.net,1433;Database=Hrushetskyi;User ID=Hrushetskyi;Password=Sive49821;Trusted_Connection=False;Encrypt=True;";
        public Manager()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connection_string);
            con.Open();

            SqlCommand comn = new SqlCommand("Select [id],[Name],[Last_name] FROM [dbo].[Manager]", con);
            SqlDataReader DR = comn.ExecuteReader();

            cmb_manager.Items.Clear();
            while (DR.Read())
            {
                managers mn = new managers();
                mn.id = (int)DR[0];
                mn.Name = DR[1].ToString() + " " + DR[2].ToString();
                cmb_manager.Items.Add(mn);
                cmb_manager.DisplayMember = "name";
                
            }
            
            con.Close();
        }

        private void cmb_manager_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bt_result_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connection_string);
            con.Open();

            SqlDataAdapter mns = new SqlDataAdapter("Select * From Manager", con);

            DataSet M_T = new DataSet();

            mns.Fill(M_T, "Manager");

            dgv_main.DataSource = M_T.Tables["Manager"];
            dgv_main.Refresh();

            con.Close();
        }

        private void btn_add_new_student_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connection_string);
            con.Open();
            try
            {
                SqlCommand comn = new SqlCommand("INSERT INTO [dbo].[Manager]([Name],[Last_name],[Age],[Phone_number])VALUES('" + txt_firstName.Text.ToString() + "','" + txt_LastName.Text.ToString() + "'," + txt_Age.Text.ToString() + "," + txt_Phone_number.Text.ToString() + ");", con);
                comn.ExecuteNonQuery();
                MessageBox.Show("Manager доданий успішно");
            }
            catch (Exception error_add)
            {
                MessageBox.Show("Помилка при додаванні в таблицю");
            }
            finally {
                con.Close();
            }
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            dgv_main.Refresh();
        }
    }
}
