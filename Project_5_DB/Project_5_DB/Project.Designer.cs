﻿namespace Project_5_DB
{
    partial class Project
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Manager_bt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Supplier = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Manager_bt
            // 
            this.Manager_bt.Location = new System.Drawing.Point(295, 155);
            this.Manager_bt.Name = "Manager_bt";
            this.Manager_bt.Size = new System.Drawing.Size(132, 59);
            this.Manager_bt.TabIndex = 0;
            this.Manager_bt.Text = "Manager";
            this.Manager_bt.UseVisualStyleBackColor = true;
            this.Manager_bt.Click += new System.EventHandler(this.Manager_bt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(193, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(391, 39);
            this.label1.TabIndex = 1;
            this.label1.Text = "Вітаємо в режимі адмін";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(175, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(423, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Виберіть таблицю яку ви хочете редагувати";
            // 
            // btn_Supplier
            // 
            this.btn_Supplier.Location = new System.Drawing.Point(295, 229);
            this.btn_Supplier.Name = "btn_Supplier";
            this.btn_Supplier.Size = new System.Drawing.Size(132, 59);
            this.btn_Supplier.TabIndex = 3;
            this.btn_Supplier.Text = "Supplier";
            this.btn_Supplier.UseVisualStyleBackColor = true;
            this.btn_Supplier.Click += new System.EventHandler(this.btn_Supplier_Click);
            // 
            // Project
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_Supplier);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Manager_bt);
            this.Name = "Project";
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Manager_bt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Supplier;
    }
}

