﻿namespace Project_5_DB
{
    partial class Supplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.txtb_Name = new System.Windows.Forms.TextBox();
            this.btn_Show = new System.Windows.Forms.Button();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.dgv_main = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtb_Country = new System.Windows.Forms.TextBox();
            this.txtb_Phone_number = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_ADD_supplier = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_main)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.btn_refresh);
            this.splitContainer1.Panel1.Controls.Add(this.btn_Show);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv_main);
            this.splitContainer1.Size = new System.Drawing.Size(971, 520);
            this.splitContainer1.SplitterDistance = 323;
            this.splitContainer1.TabIndex = 0;
            // 
            // txtb_Name
            // 
            this.txtb_Name.Location = new System.Drawing.Point(0, 53);
            this.txtb_Name.Name = "txtb_Name";
            this.txtb_Name.Size = new System.Drawing.Size(170, 22);
            this.txtb_Name.TabIndex = 0;
            // 
            // btn_Show
            // 
            this.btn_Show.Location = new System.Drawing.Point(35, 25);
            this.btn_Show.Name = "btn_Show";
            this.btn_Show.Size = new System.Drawing.Size(170, 26);
            this.btn_Show.TabIndex = 1;
            this.btn_Show.Text = "Show Supplier";
            this.btn_Show.UseVisualStyleBackColor = true;
            this.btn_Show.Click += new System.EventHandler(this.btn_Show_Click);
            // 
            // btn_refresh
            // 
            this.btn_refresh.Location = new System.Drawing.Point(35, 68);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(170, 26);
            this.btn_refresh.TabIndex = 2;
            this.btn_refresh.Text = "Refresh";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // dgv_main
            // 
            this.dgv_main.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_main.Location = new System.Drawing.Point(5, 5);
            this.dgv_main.Name = "dgv_main";
            this.dgv_main.RowHeadersWidth = 51;
            this.dgv_main.RowTemplate.Height = 24;
            this.dgv_main.Size = new System.Drawing.Size(636, 512);
            this.dgv_main.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_ADD_supplier);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtb_Phone_number);
            this.groupBox1.Controls.Add(this.txtb_Country);
            this.groupBox1.Controls.Add(this.txtb_Name);
            this.groupBox1.Location = new System.Drawing.Point(35, 164);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(170, 235);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add new Supplier";
            // 
            // txtb_Country
            // 
            this.txtb_Country.Location = new System.Drawing.Point(0, 98);
            this.txtb_Country.Name = "txtb_Country";
            this.txtb_Country.Size = new System.Drawing.Size(170, 22);
            this.txtb_Country.TabIndex = 1;
            // 
            // txtb_Phone_number
            // 
            this.txtb_Phone_number.Location = new System.Drawing.Point(0, 143);
            this.txtb_Phone_number.Name = "txtb_Phone_number";
            this.txtb_Phone_number.Size = new System.Drawing.Size(170, 22);
            this.txtb_Phone_number.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Country";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Phone number";
            // 
            // btn_ADD_supplier
            // 
            this.btn_ADD_supplier.Location = new System.Drawing.Point(-2, 183);
            this.btn_ADD_supplier.Name = "btn_ADD_supplier";
            this.btn_ADD_supplier.Size = new System.Drawing.Size(172, 25);
            this.btn_ADD_supplier.TabIndex = 5;
            this.btn_ADD_supplier.Text = "Add new Suplier";
            this.btn_ADD_supplier.UseVisualStyleBackColor = true;
            this.btn_ADD_supplier.Click += new System.EventHandler(this.btn_ADD_supplier_Click);
            // 
            // Supplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 520);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Supplier";
            this.Text = "Supplier";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_main)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.Button btn_Show;
        private System.Windows.Forms.TextBox txtb_Name;
        private System.Windows.Forms.DataGridView dgv_main;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_ADD_supplier;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtb_Phone_number;
        private System.Windows.Forms.TextBox txtb_Country;
    }
}