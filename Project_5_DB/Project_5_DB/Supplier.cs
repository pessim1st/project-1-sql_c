﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_5_DB
{
    public partial class Supplier : Form
    {
        string connection_string = "Server=tcp:itacademy.database.windows.net,1433;Database=Hrushetskyi;User ID=Hrushetskyi;Password=Sive49821;Trusted_Connection=False;Encrypt=True;";
        public Supplier()
        {
            InitializeComponent();
        }

        private void btn_Show_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connection_string);
            con.Open();

            SqlDataAdapter sns = new SqlDataAdapter("SELECT * FROM [dbo].[Supplier]", con);

            DataSet S_T = new DataSet();

            sns.Fill(S_T, "Supplier");

            dgv_main.DataSource = S_T.Tables["Supplier"];
            dgv_main.Refresh();

            con.Close();
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            dgv_main.Refresh();
        }

        private void btn_ADD_supplier_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connection_string);
            con.Open();
            try
            {
                SqlCommand comn = new SqlCommand("INSERT INTO [dbo].[Supplier]([Name],[Country],[Contract_number]) VALUES ('"+txtb_Name.Text.ToString()+"','"+txtb_Country.Text.ToString()+"',"+txtb_Phone_number.Text.ToString()+")", con);
                comn.ExecuteNonQuery();
                MessageBox.Show("Supplier доданий успішно");
            }
            catch (Exception error_add)
            {
                MessageBox.Show("Помилка при додаванні в таблицю");
            }
            finally
            {
                con.Close();
            }
        }
    }
}
